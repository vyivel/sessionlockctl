# sessionlockctl

An utility to lock and unlock Wayland compositors with ext-session-lock-v1 support.

## Building

```sh
meson setup build/
ninja -C build/
```

## Usage

```text
sessionlockctl <lock|unlock> -v
```

## License

GPL-3.0-only

See `LICENSE` for more information.

Copyright © 2023 Kirill Primak
