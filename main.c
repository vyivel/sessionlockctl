#include <assert.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
#include <wayland-util.h>

#include "ext-session-lock-v1-protocol.h"
#include "single-pixel-buffer-v1-protocol.h"
#include "viewporter-protocol.h"

enum slc_mode {
	SLC_MODE_LOCK,
	SLC_MODE_UNLOCK,
};

struct slc_output {
	struct wl_output *wl_output;
	uint32_t name;
	struct wl_list link;

	bool locked;
	struct wl_surface *wl_surface;
	struct wp_viewport *wp_viewport;
	struct ext_session_lock_surface_v1 *ext_session_lock_surface_v1;
};

static enum slc_mode mode;

static bool verbose = false;

static struct wl_compositor *wl_compositor = NULL;
static struct wp_viewporter *wp_viewporter = NULL;
static struct wp_single_pixel_buffer_manager_v1 *wp_single_pixel_buffer_manager_v1 = NULL;
static struct ext_session_lock_manager_v1 *ext_session_lock_manager_v1 = NULL;

static struct ext_session_lock_v1 *ext_session_lock_v1 = NULL;
static bool locked = false;
static bool finished = false;

static struct wl_list outputs;

static void vlog(char prefix, const char *fmt, va_list args) {
	fprintf(stderr, "[%c] ", prefix);
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
}

static void fatal(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog('!', fmt, args);
	va_end(args);
	exit(1);
}

static void info(const char *fmt, ...) {
	if (verbose) {
		va_list args;
		va_start(args, fmt);
		vlog('+', fmt, args);
		va_end(args);
	}
}

static void wl_buffer_handle_release(void *data, struct wl_buffer *wl_buffer) {
	wl_buffer_destroy(wl_buffer);
}

static struct wl_buffer_listener wl_buffer_listener = {
		.release = wl_buffer_handle_release,
};

static void ext_session_lock_surface_v1_handle_configure(void *data,
		struct ext_session_lock_surface_v1 *ext_session_lock_surface_v1, uint32_t serial,
		uint32_t width, uint32_t height) {
	struct slc_output *output = data;
	info("Committing ext_session_lock_surface_v1 for wl_output name=%" PRIu32, output->name);
	ext_session_lock_surface_v1_ack_configure(ext_session_lock_surface_v1, serial);
	struct wl_buffer *wl_buffer = wp_single_pixel_buffer_manager_v1_create_u32_rgba_buffer(
			wp_single_pixel_buffer_manager_v1, 0, 0, 0, 0);
	wl_buffer_add_listener(wl_buffer, &wl_buffer_listener, NULL);
	wp_viewport_set_destination(output->wp_viewport, width, height);
	wl_surface_attach(output->wl_surface, wl_buffer, 0, 0);
	wl_surface_commit(output->wl_surface);
}

struct ext_session_lock_surface_v1_listener ext_session_lock_surface_v1_listener = {
		.configure = ext_session_lock_surface_v1_handle_configure,
};

static void ext_session_lock_v1_handle_locked(
		void *data, struct ext_session_lock_v1 *ext_session_lock_v1) {
	locked = true;
}

static void ext_session_lock_v1_handle_finished(
		void *data, struct ext_session_lock_v1 *ext_session_lock_v1) {
	finished = true;
}

struct ext_session_lock_v1_listener ext_session_lock_v1_listener = {
		.locked = ext_session_lock_v1_handle_locked,
		.finished = ext_session_lock_v1_handle_finished,
};

static void lock_output(struct slc_output *output) {
	info("Locking wl_output name=%" PRIu32, output->name);

	assert(!output->locked);
	assert(ext_session_lock_v1 != NULL);

	output->wl_surface = wl_compositor_create_surface(wl_compositor);
	output->wp_viewport = wp_viewporter_get_viewport(wp_viewporter, output->wl_surface);
	output->ext_session_lock_surface_v1 = ext_session_lock_v1_get_lock_surface(
			ext_session_lock_v1, output->wl_surface, output->wl_output);
	ext_session_lock_surface_v1_add_listener(
			output->ext_session_lock_surface_v1, &ext_session_lock_surface_v1_listener, output);
	output->locked = true;
}

static void wl_registry_handle_global(void *data, struct wl_registry *wl_registry, uint32_t name,
		const char *interface, uint32_t version) {
	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		wl_compositor = wl_registry_bind(wl_registry, name, &wl_compositor_interface, 1);
	} else if (strcmp(interface, wp_viewporter_interface.name) == 0) {
		wp_viewporter = wl_registry_bind(wl_registry, name, &wp_viewporter_interface, 1);
	} else if (strcmp(interface, wp_single_pixel_buffer_manager_v1_interface.name) == 0) {
		wp_single_pixel_buffer_manager_v1 = wl_registry_bind(
				wl_registry, name, &wp_single_pixel_buffer_manager_v1_interface, 1);
	} else if (strcmp(interface, ext_session_lock_manager_v1_interface.name) == 0) {
		ext_session_lock_manager_v1 =
				wl_registry_bind(wl_registry, name, &ext_session_lock_manager_v1_interface, 1);
	} else if (strcmp(interface, wl_output_interface.name) == 0) {
		info("Adding wl_output name=%" PRIu32, name);
		struct slc_output *output = calloc(1, sizeof(*output));
		output->wl_output = wl_registry_bind(wl_registry, name, &wl_output_interface, 1);
		output->name = name;
		wl_list_insert(&outputs, &output->link);
		if (ext_session_lock_v1 != NULL) {
			lock_output(output);
		}
	}
}

static void wl_registry_handle_global_remove(
		void *data, struct wl_registry *wl_registry, uint32_t name) {
	struct slc_output *output;
	wl_list_for_each (output, &outputs, link) {
		if (output->name == name) {
			info("Removing wl_output name=%" PRIu32, name);
			if (output->locked) {
				ext_session_lock_surface_v1_destroy(output->ext_session_lock_surface_v1);
				wp_viewport_destroy(output->wp_viewport);
				wl_surface_destroy(output->wl_surface);
			}
			wl_list_remove(&output->link);
			free(output);
			break;
		}
	}
}

static const struct wl_registry_listener wl_registry_listener = {
		.global = wl_registry_handle_global,
		.global_remove = wl_registry_handle_global_remove,
};

static void usage(const char *name, int status) {
	fprintf(stderr, "Usage: %s <lock|unlock> [-v]\n", name);
	exit(status);
}

int main(int argc, char **argv) {
	opterr = 0;

	int opt;
	while ((opt = getopt(argc, argv, "hv")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0], 0);
			break;
		case 'v':
			verbose = true;
			break;
		default:
			usage(argv[0], 1);
			break;
		}
	}

	if (optind != argc - 1) {
		usage(argv[0], 1);
	}

	if (strcmp(argv[optind], "lock") == 0) {
		mode = SLC_MODE_LOCK;
	} else if (strcmp(argv[optind], "unlock") == 0) {
		mode = SLC_MODE_UNLOCK;
	} else {
		usage(argv[0], 1);
	}

	wl_list_init(&outputs);

	struct wl_display *wl_display = wl_display_connect(NULL);
	if (wl_display == NULL) {
		fatal("Failed to connect to a Wayland compositor");
	}
	struct wl_registry *wl_registry = wl_display_get_registry(wl_display);
	wl_registry_add_listener(wl_registry, &wl_registry_listener, NULL);
	wl_display_roundtrip(wl_display);

	if (wl_compositor == NULL) {
		fatal("Failed to bind wl_compositor");
	} else if (wp_viewporter == NULL) {
		fatal("Failed to bind wp_viewporter");
	} else if (wp_single_pixel_buffer_manager_v1 == NULL) {
		fatal("Failed to bind wp_single_pixel_buffer_manager_v1");
	} else if (ext_session_lock_manager_v1 == NULL) {
		fatal("Failed to bind ext_session_lock_manager_v1");
	}

	ext_session_lock_v1 = ext_session_lock_manager_v1_lock(ext_session_lock_manager_v1);
	ext_session_lock_v1_add_listener(ext_session_lock_v1, &ext_session_lock_v1_listener, NULL);

	wl_display_roundtrip(wl_display);

	if (finished) {
		fatal("ext_session_lock_v1 was destroyed immediately; already locked?");
	} else if (locked) {
		info("Locked without creating surfaces");
	} else {
		struct slc_output *output;
		wl_list_for_each (output, &outputs, link) {
			lock_output(output);
		}

		info("Waiting for the session to be locked");

		while (wl_display_dispatch(wl_display) != -1) {
			if (locked || finished) {
				break;
			}
		}

		if (finished) {
			fatal("ext_session_lock_v1 was destroyed unexpectedly");
		} else if (!locked) {
			fatal("Failed to lock the session");
		}

		info("Session locked");
	}

	switch (mode) {
	case SLC_MODE_LOCK:
		info("Exiting with the session locked");
		break;
	case SLC_MODE_UNLOCK:
		info("Unlocking the session");
		ext_session_lock_v1_unlock_and_destroy(ext_session_lock_v1);
		wl_display_roundtrip(wl_display);
		break;
	}

	return 0;
}
